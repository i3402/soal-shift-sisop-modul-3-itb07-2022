#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

void exec(char *cmd, char *argv[])
{
  pid_t id_child;
  id_child = fork();
  int status;

  if (id_child == 0)
  {
    execv(cmd, argv);
  }

  else
  {
    while (wait(&status) > 0)
      ;
  }
}

pthread_t tid1, tid2, tid3;
char *ciphertext[216];

size_t b64_decoded_size(const char *in)
{
  size_t len;
  size_t ret;
  size_t i;

  if (in == NULL)
    return 0;

  len = strlen(in);
  ret = len / 4 * 3;

  for (i = len; i-- > 0;)
  {
    if (in[i] == '=')
    {
      ret--;
    }
    else
    {
      break;
    }
  }

  return ret;
}

int b64invs[] = {62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
                 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
                 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
                 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
                 43, 44, 45, 46, 47, 48, 49, 50, 51};

int b64_isvalidchar(char c)
{
  if (c >= '0' && c <= '9')
    return 1;
  if (c >= 'A' && c <= 'Z')
    return 1;
  if (c >= 'a' && c <= 'z')
    return 1;
  if (c == '+' || c == '/' || c == '=')
    return 1;
  return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
  size_t len;
  size_t i;
  size_t j;
  int v;

  if (in == NULL || out == NULL)
    return 0;

  len = strlen(in);
  if (outlen < b64_decoded_size(in) || len % 4 != 0)
    return 0;

  for (i = 0; i < len; i++)
  {
    if (!b64_isvalidchar(in[i]))
    {
      return 0;
    }
  }

  for (i = 0, j = 0; i < len; i += 4, j += 3)
  {
    v = b64invs[in[i] - 43];
    v = (v << 6) | b64invs[in[i + 1] - 43];
    v = in[i + 2] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 2] - 43];
    v = in[i + 3] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 3] - 43];

    out[j] = (v >> 16) & 0xFF;
    if (in[i + 2] != '=')
      out[j + 1] = (v >> 8) & 0xFF;
    if (in[i + 3] != '=')
      out[j + 2] = v & 0xFF;
  }

  return 1;
}

char *startDecode(char *data)
{
  char *out;
  size_t out_len;

  // printf("data:    '%s'\n", data);

  /* +1 for the NULL terminator. */
  out_len = b64_decoded_size(data) + 1;
  out = malloc(out_len);

  if (!b64_decode(data, (unsigned char *)out, out_len))
  {
    printf("Decode Failure\n");
    // return 1;
  }
  out[out_len] = '\0';

  // printf("dec:     '%s'\n", out);

  return out;
  free(out);
}

char *read_file(char *filename)
{
  FILE *file;

  file = fopen(filename, "r");

  if (file == NULL)
    return NULL;

  fseek(file, 0, SEEK_END);

  int length = ftell(file);

  fseek(file, 0, SEEK_SET);

  char *string = malloc(sizeof(char) * (length + 1));

  char c;

  int i = 0;

  while ((c = fgetc(file)) != EOF)
  {
    string[i] = c;

    i++;
  }

  string[i] = '\0';

  // strcpy(ciphertext[p], string);

  return string;
}

void *zipLagi()
{
  char *iniZip[] = {"zip", "-q", "-P", "mihinomenestafrida", "-r", "hasil.zip", "-m", "hasil", "no.txt", NULL};
  exec("/bin/zip", iniZip);
}

void *tulisIsi()
{
  FILE *fp = fopen("no.txt", "w");
  fprintf(fp, "No");
  fclose(fp);
}

void *tambahFile()
{
  char *iniFile[] = {"touch", "no.txt", NULL};
  exec("/bin/touch", iniFile);
}

void *unzipFolder()
{
  char *iniUnzip[] = {"unzip", "-q", "hasil.zip", NULL};
  exec("/bin/unzip", iniUnzip);
}

void *zipFolder()
{
  char *iniZip[] = {"zip", "-q", "-P", "mihinomenestafrida", "-r", "hasil.zip", "-m", "hasil", NULL};
  exec("/bin/zip", iniZip);
}

void *pindahQuote()
{
  char *iniPindah[] = {"mv", "/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", "/home/afridarn/SisOp/modul3/soal1/hasil/", NULL};
  exec("/bin/mv", iniPindah);
}

void *pindahMusic()
{
  char *iniPindah[] = {"mv", "/home/afridarn/SisOp/modul3/soal1/music/music.txt", "/home/afridarn/SisOp/modul3/soal1/hasil/", NULL};
  exec("/bin/mv", iniPindah);
}

void *decodeQuote()
{
  char *dm1 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q1.txt");
  char *decoded = startDecode(dm1);
  char *dm2 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q2.txt");
  char *decoded2 = startDecode(dm2);
  char *dm3 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q3.txt");
  char *decoded3 = startDecode(dm3);
  char *dm4 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q4.txt");
  char *decoded4 = startDecode(dm4);
  char *dm5 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q5.txt");
  char *decoded5 = startDecode(dm5);
  char *dm6 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q6.txt");
  char *decoded6 = startDecode(dm6);
  char *dm7 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q7.txt");
  char *decoded7 = startDecode(dm7);
  char *dm8 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q8.txt");
  char *decoded8 = startDecode(dm8);
  char *dm9 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q9.txt");
  char *decoded9 = startDecode(dm9);
  FILE *output = fopen("/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", "a");
  fprintf(output, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s", decoded, decoded2, decoded3, decoded4, decoded5, decoded6, decoded7, decoded8, decoded9);
  fclose(output);
}

void *decodeMusic()
{
  char *dm1 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m1.txt");
  char *decoded = startDecode(dm1);
  char *dm2 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m2.txt");
  char *decoded2 = startDecode(dm2);
  char *dm3 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m3.txt");
  char *decoded3 = startDecode(dm3);
  char *dm4 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m4.txt");
  char *decoded4 = startDecode(dm4);
  char *dm5 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m5.txt");
  char *decoded5 = startDecode(dm5);
  char *dm6 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m6.txt");
  char *decoded6 = startDecode(dm6);
  char *dm7 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m7.txt");
  char *decoded7 = startDecode(dm7);
  char *dm8 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m8.txt");
  char *decoded8 = startDecode(dm8);
  char *dm9 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m9.txt");
  char *decoded9 = startDecode(dm9);
  FILE *output = fopen("/home/afridarn/SisOp/modul3/soal1/music/music.txt", "a");
  fprintf(output, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s", decoded, decoded2, decoded3, decoded4, decoded5, decoded6, decoded7, decoded8, decoded9);
  fclose(output);
}

void *unzipQuote()
{
  char *iniUnzip[] = {"unzip", "-q", "dbquote", "-d", "/home/afridarn/SisOp/modul3/soal1/quote", NULL};
  exec("/bin/unzip", iniUnzip);
}

void *unzipMusic()
{
  char *iniUnzip[] = {"unzip", "-q", "dbmusic", "-d", "/home/afridarn/SisOp/modul3/soal1/music", NULL};
  exec("/bin/unzip", iniUnzip);
}

void *downloadQuote()
{
  char *iniQuote[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "dbquote", NULL};
  exec("/bin/wget", iniQuote);
  char *iniUnzip[] = {"unzip", "-q", "dbquote", "-d", "/home/afridarn/SisOp/modul3/soal1/quote", NULL};
  exec("/bin/unzip", iniUnzip);
  char *iniTouch[] = {"touch", "/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", NULL};
  exec("/bin/touch", iniTouch);
}

void *downloadMusic()
{
  char *iniMusic[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "dbmusic", NULL};
  exec("/bin/wget", iniMusic);
  char *iniUnzip[] = {"unzip", "-q", "dbmusic", "-d", "/home/afridarn/SisOp/modul3/soal1/music", NULL};
  exec("/bin/unzip", iniUnzip);
  char *iniTouch[] = {"touch", "/home/afridarn/SisOp/modul3/soal1/music/music.txt", NULL};
  exec("/bin/touch", iniTouch);
}

void *bikinFolder()
{
  char *folderHasil[] = {"mkdir", "hasil", NULL};
  exec("/bin/mkdir", folderHasil);
  char *folderQuote[] = {"mkdir", "quote", NULL};
  exec("/bin/mkdir", folderQuote);
  char *folderMusic[] = {"mkdir", "music", NULL};
  exec("/bin/mkdir", folderMusic);
}

int main()
{
  pthread_t tid1, tid2, tid3, tid4, tid5;

  bikinFolder();
  pthread_create(&tid1, NULL, &downloadMusic, NULL);
  pthread_create(&tid2, NULL, &downloadQuote, NULL);
  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  decodeMusic();
  decodeQuote();
  pthread_create(&tid3, NULL, &pindahMusic, NULL);
  pthread_create(&tid4, NULL, &pindahQuote, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  zipFolder();
  unzipFolder();
  tambahFile();
  tulisIsi();
  zipLagi();

  return 0;
}
