# soal-shift-sisop-modul-2-ITB07-2022

## Laporan Resmi Modul 2 Praktikum Sistem Operasi 2022
---
### Kelompok ITB07:
Calvindra Laksmono Kumoro - 5027201020  
Afrida Rohmatin Nuriyah - 5027201037  
Brilianti Puspita Sari - 5027201070  

---
# Soal 1  
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.  
**Fungsi main**
```c
int main()
{
  pthread_t tid1, tid2, tid3, tid4, tid5;

  bikinFolder();
  pthread_create(&tid1, NULL, &downloadMusic, NULL);
  pthread_create(&tid2, NULL, &downloadQuote, NULL);
  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  decodeMusic();
  decodeQuote();
  pthread_create(&tid3, NULL, &pindahMusic, NULL);
  pthread_create(&tid4, NULL, &pindahQuote, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  zipFolder();
  unzipFolder();
  tambahFile();
  tulisIsi();
  zipLagi();

  return 0;
}
```
## 1A
**Analisa Soal**  
Untuk nomor 1 ini diawali dengan mendownload dua file zip `(music dan quote)` kemudian mengunzip kedua file tersebut ke dalam dua folder yang berbeda. Folder `music` untuk music.zip dan folder `quote` untuk quote.zip.  
**Penyelesaian**  
Sebelum mendownload dan mengunzip, kami membuat 3 folder terlebih dahulu. 2 untuk hasil unzip dan 1 untuk hasil decode di akhir. Kodenya sebagai berikut:
```c
void *bikinFolder()
{
  char *folderHasil[] = {"mkdir", "hasil", NULL};
  exec("/bin/mkdir", folderHasil);
  char *folderQuote[] = {"mkdir", "quote", NULL};
  exec("/bin/mkdir", folderQuote);
  char *folderMusic[] = {"mkdir", "music", NULL};
  exec("/bin/mkdir", folderMusic);
}
``` 
Langkah selanjutnya yaitu melakukan download dan unzip file `music.zip` dengan kode:
```c
void *downloadMusic()
{
  char *iniMusic[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "dbmusic", NULL};
  exec("/bin/wget", iniMusic);
  char *iniUnzip[] = {"unzip", "-q", "dbmusic", "-d", "/home/afridarn/SisOp/modul3/soal1/music", NULL};
  exec("/bin/unzip", iniUnzip);
  char *iniTouch[] = {"touch", "/home/afridarn/SisOp/modul3/soal1/music/music.txt", NULL};
  exec("/bin/touch", iniTouch);
}
``` 
Untuk mendownload dan melakukan unzip pada file `quote.zip` dibuat kode sebagai berikut:
```c
void *downloadQuote()
{
  char *iniQuote[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "dbquote", NULL};
  exec("/bin/wget", iniQuote);
  char *iniUnzip[] = {"unzip", "-q", "dbquote", "-d", "/home/afridarn/SisOp/modul3/soal1/quote", NULL};
  exec("/bin/unzip", iniUnzip);
  char *iniTouch[] = {"touch", "/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", NULL};
  exec("/bin/touch", iniTouch);
}
``` 
### Output soal 1A  
![image](./img/1a.JPG) 

## 1B  
**Analisa Soal**  
Pada soal ini diminta untuk melakukan decode dengan menggunakan `base64` pada semua file txt hasil unzip dari kedua folder sebelumnya. Kemudian, hasil decode dimasukkan ke dalam satu file .txt baru:  
1. File music.txt untuk hasil decode dari folder music
2. File quote.txt untuk hasil decode dari folder quote  

Setiap kalimat dipisahkan dengan `enter/newline`.  
**Penyelesaian**  
Untuk melakukan decode pada file .txt yang ada di folder music, pertama dilakukan pengambilan masing-masing isi file .txt dengan menggunakan function `read_file` dan simpan pada variabel `dm`. Setelah itu dilakukan decode dengan menggunakan function `startDecode` dan disimpan pada variabel `decoded`. Setelah semua file selesai di-decode maka dapat diprint pada file `music.txt` (telah dibuat pada 1A) dengan dipisahkan newline.
```c
void *decodeMusic()
{
  char *dm1 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m1.txt");
  char *decoded = startDecode(dm1);
  char *dm2 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m2.txt");
  char *decoded2 = startDecode(dm2);
  char *dm3 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m3.txt");
  char *decoded3 = startDecode(dm3);
  char *dm4 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m4.txt");
  char *decoded4 = startDecode(dm4);
  char *dm5 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m5.txt");
  char *decoded5 = startDecode(dm5);
  char *dm6 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m6.txt");
  char *decoded6 = startDecode(dm6);
  char *dm7 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m7.txt");
  char *decoded7 = startDecode(dm7);
  char *dm8 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m8.txt");
  char *decoded8 = startDecode(dm8);
  char *dm9 = read_file("/home/afridarn/SisOp/modul3/soal1/music/m9.txt");
  char *decoded9 = startDecode(dm9);
  FILE *output = fopen("/home/afridarn/SisOp/modul3/soal1/music/music.txt", "a");
  fprintf(output, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s", decoded, decoded2, decoded3, decoded4, decoded5, decoded6, decoded7, decoded8, decoded9);
  fclose(output);
}
``` 
Fungsi `read_file` :
```c
char *read_file(char *filename)
{
  FILE *file;

  file = fopen(filename, "r");

  if (file == NULL)
    return NULL;

  fseek(file, 0, SEEK_END);

  int length = ftell(file);

  fseek(file, 0, SEEK_SET);

  char *string = malloc(sizeof(char) * (length + 1));

  char c;

  int i = 0;

  while ((c = fgetc(file)) != EOF)
  {
    string[i] = c;

    i++;
  }

  string[i] = '\0';

  // strcpy(ciphertext[p], string);

  return string;
}
``` 
Fungsi `startDecode` :
```c
char *startDecode(char *data)
{
  char *out;
  size_t out_len;

  // printf("data:    '%s'\n", data);

  /* +1 for the NULL terminator. */
  out_len = b64_decoded_size(data) + 1;
  out = malloc(out_len);

  if (!b64_decode(data, (unsigned char *)out, out_len))
  {
    printf("Decode Failure\n");
    // return 1;
  }
  out[out_len] = '\0';

  // printf("dec:     '%s'\n", out);

  return out;
  free(out);
}
``` 
Fungsi `b64_decode` :
```c
int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
  size_t len;
  size_t i;
  size_t j;
  int v;

  if (in == NULL || out == NULL)
    return 0;

  len = strlen(in);
  if (outlen < b64_decoded_size(in) || len % 4 != 0)
    return 0;

  for (i = 0; i < len; i++)
  {
    if (!b64_isvalidchar(in[i]))
    {
      return 0;
    }
  }

  for (i = 0, j = 0; i < len; i += 4, j += 3)
  {
    v = b64invs[in[i] - 43];
    v = (v << 6) | b64invs[in[i + 1] - 43];
    v = in[i + 2] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 2] - 43];
    v = in[i + 3] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 3] - 43];

    out[j] = (v >> 16) & 0xFF;
    if (in[i + 2] != '=')
      out[j + 1] = (v >> 8) & 0xFF;
    if (in[i + 3] != '=')
      out[j + 2] = v & 0xFF;
  }

  return 1;
}
```  
Fungsi untuk menghitung panjang kalimat yang akan didecode:
```c
size_t b64_decoded_size(const char *in)
{
  size_t len;
  size_t ret;
  size_t i;

  if (in == NULL)
    return 0;

  len = strlen(in);
  ret = len / 4 * 3;

  for (i = len; i-- > 0;)
  {
    if (in[i] == '=')
    {
      ret--;
    }
    else
    {
      break;
    }
  }

  return ret;
}
```  
Selanjutnya, untuk melakukan decode pada file .txt yang ada di folder quote, pertama dilakukan pengambilan masing-masing isi file .txt dengan menggunakan function `read_file` dan simpan pada variabel `dm`. Setelah itu dilakukan decode dengan menggunakan function `startDecode` dan disimpan pada variabel `decoded`. Setelah semua file selesai di-decode maka dapat diprint pada file `quote.txt` (telah dibuat pada 1A) dengan dipisahkan newline.  
```c
void *decodeQuote()
{
  char *dm1 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q1.txt");
  char *decoded = startDecode(dm1);
  char *dm2 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q2.txt");
  char *decoded2 = startDecode(dm2);
  char *dm3 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q3.txt");
  char *decoded3 = startDecode(dm3);
  char *dm4 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q4.txt");
  char *decoded4 = startDecode(dm4);
  char *dm5 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q5.txt");
  char *decoded5 = startDecode(dm5);
  char *dm6 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q6.txt");
  char *decoded6 = startDecode(dm6);
  char *dm7 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q7.txt");
  char *decoded7 = startDecode(dm7);
  char *dm8 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q8.txt");
  char *decoded8 = startDecode(dm8);
  char *dm9 = read_file("/home/afridarn/SisOp/modul3/soal1/quote/q9.txt");
  char *decoded9 = startDecode(dm9);
  FILE *output = fopen("/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", "a");
  fprintf(output, "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s", decoded, decoded2, decoded3, decoded4, decoded5, decoded6, decoded7, decoded8, decoded9);
  fclose(output);
}
```
Untuk fungsi `read_file` dan `startDecode` menggunakan function yang sama seperti sebelumnya.
### Output soal 1B
Isi file `music.txt`:
![image](./img/1b_1.JPG)  
Isi file `quote.txt`:
![image](./img/1b_2.JPG) 
## 1C
**Analisa Soal**
Pada 1C diminta untuk memindahkan kedua file hasil decode `(music.txt dan quote.txt)` ke dalam folder `hasil`.  
**Penyelesaian**  
Untuk memindahkan file music.txt tersebut dibuat kode:
```c
void *pindahMusic()
{
  char *iniPindah[] = {"mv", "/home/afridarn/SisOp/modul3/soal1/music/music.txt", "/home/afridarn/SisOp/modul3/soal1/hasil/", NULL};
  exec("/bin/mv", iniPindah);
}
```
Sedangkan untuk memindahkan file quote.txt dapat dijalankan dengan kode:
```c
void *pindahQuote()
{
  char *iniPindah[] = {"mv", "/home/afridarn/SisOp/modul3/soal1/quote/quote.txt", "/home/afridarn/SisOp/modul3/soal1/hasil/", NULL};
  exec("/bin/mv", iniPindah);
}
```  
### Output soal 1C  
Isi dari folder hasil:
![image](./img/1c.JPG)  
Pada folder `music` sudah tidak terdapat file `music.txt`:
![image](./img/1c_2.JPG)  
Pada folder `quote` sudah tidak terdapat file `quote.txt`:
![image](./img/1c_3.JPG)
## 1D  
**Analisis Soal**  
Setelah dipindahkan ke folder hasil, folder tersebut di-zip menjadi `hasil.zip` dengan `password: mihinomenest[namauser]` dengan nama user adalah nama salah satu anggota kelompok.   
**Penyelesaian**  
Dengan mengambil nama salah satu anggota kelompok, maka password dari hasil.zip adalah `mihonomenestafrida`. Dan untuk kode penyelesainnya sebagai berikut:
```c
void *zipFolder()
{
  char *iniZip[] = {"zip", "-q", "-P", "mihinomenestafrida", "-r", "hasil.zip", "-m", "hasil", NULL};
  exec("/bin/zip", iniZip);
}
```
### Output Soal 1D
`hasil.zip` yang telah dipassword:
![image](./img/1d.JPG)  
Isi dari `hasil.zip`:  
![image](./img/1d_2.JPG)
## 1E  
**Analisis Soal**  
Pada soal ini diminta untuk melakukan unzip dan membuat file baru bernama `no.txt` yang berisikan `No`. Kemudian, folder `hasil` dan file `no.txt` di-zip kembali dengan menggunakan password yang sama seperti sebelumnya.  
**Penyelesaian**  
Untuk melakukan unzip dibuat kode sebagai berikut:
```c
void *unzipFolder()
{
  char *iniUnzip[] = {"unzip", "-q", "hasil.zip", NULL};
  exec("/bin/unzip", iniUnzip);
}
```
Lalu, untuk membuat file `no.txt` dan mengisinya, dapat dijalankan dengan kode:
```c
void *tambahFile()
{
  char *iniFile[] = {"touch", "no.txt", NULL};
  exec("/bin/touch", iniFile);
}

void *tulisIsi()
{
  FILE *fp = fopen("no.txt", "w");
  fprintf(fp, "No");
  fclose(fp);
}
```
Kemudian, dilakukan zip kembali menggunakan kode:
```c
void *zipLagi()
{
  char *iniZip[] = {"zip", "-q", "-P", "mihinomenestafrida", "-r", "hasil.zip", "-m", "hasil", "no.txt", NULL};
  exec("/bin/zip", iniZip);
}
```
### Output Soal 1E
Setelah di-unzip dan dibuat file `no.txt`:
![image](./img/1e.JPG)  
Isi dari file `no.txt`:
![image](./img/1e_2.JPG)
`hasil.zip` dari folder `hasil` dan file `no.txt` yang telah dipassword:
![image](./img/1e_3.JPG)  
Isi dari file `hasil.zip`
![image](./img/1e_4.JPG)  
### Kendala yang dihadapi  
Untuk kendala pada nomor 1, kami sempat kesusahan dalam melakukan decoding dan memasukkannya ke dalam file baru. Selain itu, kami juga sempat kesusahan dalam menggunaka pthread_join.
#  Soal 2
## Exe. 2a
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

users.txt
username:password
username2:password2

### Analisa Soal
pada soal ini diminta untuk membuat fungsi login dan regsiter yang akan terhubung ke dalam server, lalu akun yang berhasil login dan register akan masuk ke dalam users.txt

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)
```c
//* Maximum clients
#define MAX_CLI 10
//* Buffer size
#define BUFF_SZ 1024
//* Race condition safe client count
static _Atomic int cli_count = 0;
//* Initial uid
static int uid = 10;
```
#### Fungsi Main Server (Main Thread)

```c
char* ip = "127.0.0.1";
  int port = 6969;
  int opt = 1;
  int listenfd = 0, connfd = 0;
  struct sockaddr_in serv_addr, cli_addr;
  errno = 0;
  pthread_t tid;
  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  if (listenfd < 0) error("ERROR: Error opening socket!");
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(ip);;
  serv_addr.sin_port = htons(port);
```

Server memeriksa apakah file `akun.txt`dan `problem tsv` ada dalam server, jika ada maka diakses, jika tidak maka dibuat.

```c
  while (1) {
    socklen_t clilen = sizeof(cli_addr);
    //* Every new connection is stored in connfd first
    connfd = accept(listenfd, (struct sockaddr*)&cli_addr, &clilen);
    if ((cli_count + 1) == MAX_CLI) {
      printf("Max clients reached. Rejected: ");
      printf(":%d\n", cli_addr.sin_port);
      close(connfd);
      continue;
    }
    //* Client settings
    client_t* cli = (client_t*)malloc(sizeof(client_t));
    cli->addr = cli_addr;
    cli->sockfd = connfd;
    cli->uid = uid++;
    //* Add client to the queue and create new thread
    add_client(cli);
    pthread_create(&tid, NULL, &handle_client, (void*)cli);
    sleep(1);
  }
```

Infinite loop agar server dapat menerima client baru lalu, menambahkan ke queue, dan membuat thread baru sesuai dengan jumlah client yang ingin menyambungkan diri.

#### `client_t` struct

```C
typedef struct {
  struct sockaddr_in addr;
  int sockfd;
  int uid;
  char credentials[BUFF_SZ];
} client_t;
```

#### Fungsi Main Client (Main Thread)

```C
char *ip = "127.0.0.1";
int port = 6969;
signal(SIGINT, handle_exit);
struct sockaddr_in serv_addr;
//* Socket settings
sockfd = socket(AF_INET, SOCK_STREAM, 0);
serv_addr.sin_family = AF_INET;
serv_addr.sin_addr.s_addr = inet_addr(ip);
serv_addr.sin_port = htons(port);
int conn = connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
if (conn < 0) {
    error("ERROR: connect\n");
}
```

Client akan menjalankan socket lalu **mencoba** `connect` ke `127.0.0.1:6969`

```C
pthread_t send_thread, rcv_thread;
//* Handling send to server
if (pthread_create(&send_thread, NULL, (void *)send_handler, NULL) != 0) {
    error("Error pthread send.");
}
//* Handling receive from server
if (pthread_create(&rcv_thread, NULL, (void *)recv_handler, NULL) != 0) {
    error("Error pthread recv.");
}
```

### Kendala
pada awal pengerjaan terdapat eror saat meng koneksi kan client dan server, namun sudah berhasil

![](Dokumentasi03/img1.png)
![](Dokumentasi03/img2.png)

## Exe. 2b
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)
```c 
printf("problem: /t");
  scanf("%s", pub);
  printf("author: /t");
  scanf("%s", year);
  printf("Filepath: /t");
  scanf("%s", path);
```

### Kendala
tidak ditemukan kendala
![](Dokumentasi03/img3.png)

## Exe 2c
Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
	Contoh:
	Client-side
add

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)


Jika client mengirimkan `add` maka console di client akan mengeluarkan:
```
problem:
author:
Filepath:
```

```c
void add_command() {
  char pub[256];
  char year[10];
  char path[512];
  char filename[512];

  printf("problem: /t");
  scanf("%s", pub);
  printf("author: /t");
  scanf("%s", year);
  printf("Filepath: /t");
  scanf("%s", path);

  //* Inform the server you're going to add
  strcpy(buffer, "add");
  send(sockfd, buffer, strlen(buffer), 0);
```

fungsi ini dugunakan untuk menggunakan command add pada server, dan langsung diinputkan pada proble tsv

### Kendala
tidak ditemukan kendala
![](Dokumentasi03/img4.png)
![](Dokumentasi03/img5.png)




## Exe 2d
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)

Pada saat client mengirimkan command `see` maka server akan menghandle nya di fungsi [client_see()] Lalu client akan membaca isi dari `problem.tsv` lalu mengirimkannya ke client dengan bantuan fungsi `pretty_print()`
```C
void pretty_print(char* line, client_t* cli) {
  char buffer[BUFF_SZ] = {0};
  char filename[256] = {0};
  char fileext[6] = {0};
  char pub[256] = {0};
  char year[6] = {0};
  char filepath[256] = {0};
  int n;
  //* Split by tab
  sscanf(line, "%s\t%s\t%s", filepath, pub, year);
  //* remove FILES/
  bzero(buffer, BUFF_SZ);
  strcpy(buffer, filepath);
  char* path_token = strtok(buffer, "/");
  path_token = strtok(NULL, "/");  //* filename.ext
  // *split filename and ext
  char* name_ext_token = strtok(path_token, ".");
  strcpy(filename, name_ext_token);
  name_ext_token = strtok(NULL, ".");
  strcpy(fileext, name_ext_token);
  bzero(buffer, BUFF_SZ);
  sprintf(buffer,
          "Nama: %s\n"
          "Publisher: %s\n"
          "Tahun Publishing: %s\n"
          "Extensi File: %s\n"
          "Filepath: %s\n\n",
          filename, pub, year, fileext, filepath);
  n = send(cli->sockfd, buffer, BUFF_SZ, 0);
}
```

### Kendala
tidak ditemukan kendala
![](Dokumentasi03/img6.png)


## Exe 2e
Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)

Pada saat client mengirimkan command `download` maka server akan menghandle nya di fungsi [client_download()] Pertama-tama server akan memeriksa `problem.tsv` dengan bantuan fungsi `find_file()` lalu jika file valid, maka server akan mengirimkan file tersebut dengan bantuan fungsi `send_file()`
```C
int find_file(char* filename) {
  FILE* db = fopen("files.tsv", "r");
  char buffer[BUFF_SZ] = {0};
  char filepath[512] = {0};
  sprintf(filepath, "FILES/%s", filename);
  printf("Finding %s...\n", filepath);
  bzero(buffer, BUFF_SZ);
  int line = 0;
  while (fscanf(db, "%s\t%*s\t%*s", buffer) != EOF) {
    line++;
    if (strcmp(filepath, buffer) == 0) {
      fclose(db);
      return line;
    }
  }
  fclose(db);
  return -1;
}
...
void client_download(client_t* cli) {
...
if (is_valid) {
    printf("downloading %s\n", filepath);
    n = send(cli->sockfd, "code_found", 15, 0);
    send_file(filepath, cli->sockfd);
  } else {
    printf("%s is not found\n", filename);
    n = send(cli->sockfd, "code_err", 10, 0);
  }
...
```
Jika fungsi ini mereturn `-1` maka server akan mengirimkan `File not found!` ke client.

### Kendala
tidak ditemukan 


## Exe 2g
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)

```c
  while (1) {
    socklen_t clilen = sizeof(cli_addr);
    //* Every new connection is stored in connfd first
    connfd = accept(listenfd, (struct sockaddr*)&cli_addr, &clilen);
    if ((cli_count + 1) == MAX_CLI) {
      printf("Max clients reached. Rejected: ");
      printf(":%d\n", cli_addr.sin_port);
      close(connfd);
      continue;
    }
    //* Client settings
    client_t* cli = (client_t*)malloc(sizeof(client_t));
    cli->addr = cli_addr;
    cli->sockfd = connfd;
    cli->uid = uid++;
    //* Add client to the queue and create new thread
    add_client(cli);
    pthread_create(&tid, NULL, &handle_client, (void*)cli);
    sleep(1);
  }
```

Infinite loop agar server dapat menerima client baru lalu, menambahkan ke queue, dan membuat thread baru sesuai dengan jumlah client yang ingin menyambungkan diri.

#### `client_t` struct

```C
typedef struct {
  struct sockaddr_in addr;
  int sockfd;
  int uid;
  char credentials[BUFF_SZ];
} client_t;
```

Struct ini berisi addres, socket fd, uid, dan credentials (`id:password`) dari client

#### Global Variables Client

```C
#define BUFF_SZ 1024
volatile sig_atomic_t flag = 0;
/** Storing server status
 * 0 means server is busy
 * 1 means server is ready to serve */
int server_avail = 0;
//* Storing auth status of this client
int is_logged_in = 0;
int sockfd = 0;
char id[32];
char pass[32];
char buffer[BUFF_SZ] = {0};
```

---
## Soal 3
ITB07

## Soal 3a
## Analisa soal 3a:
Pada nomor 3 pertama diminta untuk bisa mengextract zip yang ada pada directory  “/home/user/shift3/”. Kemudian memindahkan program soal3.c ke dalam directory harta karun yang sudah di extract untuk bisa dikategorikan

## Cara pengerjaan 3a:
Pada soal ini diminta untuk mengextract zip harta karun pada “/home/user/shift3/” dengan cara menggunakan extract here yang ada pada linux. Kemudian memindahkan secara manual program soal3.c ke dalam directory “/home/user/shift3/harta karun”. Dan seluruh file akan dikategorikan secara rekursif dengan menggunakan fungsi memindahkan file ini
```c
 void *pindahFile(void *arg){
  char str[999];
  char buffer[999];
  char buffer2[999];
  char buffer3[999];
  char buffer4[999];
  char path[1000];
  char tempDest[1000];
  char cwd[1000];
  char fileName[1000];
  char fileExt[1000];
  char fileExt2[1000];

  getcwd(cwd, sizeof(cwd));
  strcpy(path, (char*) arg);
  int isFile = isRegular(path);

  if(access(path, F_OK) == -1){
      // printf("File %s: Sad, gagal:(\n", tempCurr);
      // pthread_exit(0);
      return (void *) 0;
  }

  if(!isFile){
    // printf("File %s: Sad, gagal:(\n", tempCurr);
    return (void *) 0;
    // pthread_exit(0);
  }

  strcpy(buffer4, path);

  char *fileExt3;
  char dot1 = '.';
  fileExt3 = strchr(buffer4, dot1);
  // printf("%s", fileExt3);

  strcpy(buffer, path);
  char *token=strtok(buffer, "/");
  while(token != NULL){
      sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }

  strcpy(buffer, path);
  strcpy(buffer2, fileName);
  strcpy(buffer3, fileName);
  // strcpy(buffer4, fileName);


  int count = 0;

  char *token2=strtok(buffer2, ".");
  // printf("%s", token2);
  sprintf(fileExt2, "%s", token2);
 }
```

Dan untuk fungsi untuk bisa memindahkan dengan rekursif, menggunakan fungsi ini.
```c
void listFilesRecursively(char *basePath, int *iter)
{
    // int iter = 0;
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(basePath);

  if (!dir){
      
      return;

  }

  while ((dp = readdir(dir)) != NULL)
  {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
      {

        strcpy(path, basePath);
        strcat(path, "/");
        strcat(path, dp->d_name);

        if(isRegular(path)){
          strcpy(keepFile[*iter], path);
          *iter = *iter + 1;
        }
        else{
          listFilesRecursively(path, iter);
        }

      }
  }
    closedir(dir);
}
```

##  Hasil Run 3a
![user.txt](./img/3a1.JPG)
![user.txt](./img/3a2.JPG)

## Kendala 3a: 
Untuk kendala pada nomor 3a ini yaitu tidak boleh menggunakan exec mv untuk memindahkan file sehingga bobot soal menjadi lebih berat

## Soal 3b
## Analisa Soal 3b:
Pada soal ini kita diminta untuk bisa mengkategorikan file yang tidak memiliki ekstensi ke dalam folder “Unknown” dan file hidden ke dalam folder “Hidden”.


## Cara pengerjaan 1b:
Pada program ini memindahkan jenis gambar tersebut ke dalam folder tersebut dengan menggunakan fungsi pindah file.

```c
void *pindahFile(void *arg){
  char str[999];
  char buffer[999];
  char buffer2[999];
  char buffer3[999];
  char buffer4[999];
  char path[1000];
  char tempDest[1000];
  char cwd[1000];
  char fileName[1000];
  char fileExt[1000];
  char fileExt2[1000];

  getcwd(cwd, sizeof(cwd));
  strcpy(path, (char*) arg);
  int isFile = isRegular(path);

  if(access(path, F_OK) == -1){
      // printf("File %s: Sad, gagal:(\n", tempCurr);
      // pthread_exit(0);
      return (void *) 0;
  }

  if(!isFile){
    // printf("File %s: Sad, gagal:(\n", tempCurr);
    return (void *) 0;
    // pthread_exit(0);
  }

  strcpy(buffer4, path);

  char *fileExt3;
  char dot1 = '.';
  fileExt3 = strchr(buffer4, dot1);
  // printf("%s", fileExt3);

  strcpy(buffer, path);
  char *token=strtok(buffer, "/");
  while(token != NULL){
      sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }

  strcpy(buffer, path);
  strcpy(buffer2, fileName);
  strcpy(buffer3, fileName);
  // strcpy(buffer4, fileName);


  int count = 0;

  char *token2=strtok(buffer2, ".");
  // printf("%s", token2);
  sprintf(fileExt2, "%s", token2);

  
  while(token2 != NULL){
      count++;
      // printf("%d", count);
    //   printf("%s\n", token2);
      sprintf(fileExt, "%s", token2);
    //   printf("%s", fileExt);
      token2=strtok(NULL, ".");
    }
    // printf("%s", fileExt);
  char dot = '.';
  char first = buffer3[0];
//   printf("%s", fileExt2);
    // printf("%c", buffer3[0]);
  if(dot == first){
    strcpy(fileExt, "Hidden");
  }

  else if(count >= 3){
    strcpy(fileExt, fileExt3+1);
  }

  else if (count <=1 ){
    strcpy(fileExt, "Unknown");
  }

  for (int i = 0; i < sizeof(fileExt); i++){
      fileExt[i] = tolower(fileExt[i]);
  }

  strcpy(buffer, (char*) arg);
  mkdir(fileExt, 0777);

  strcat(cwd, "/");
  strcat(cwd,fileExt);
  strcat(cwd, "/");
  strcat(cwd, fileName);
  strcpy(tempDest, cwd);

    
  rename(buffer, tempDest);
   
  return (void *) 1;
  

}

```
## Hasil Run 3b:
![user.txt](./img/3b1.JPG)

## Kendala 3b:
Untuk kendala nomor 3b ini yaitu pada bobot soal yang lumayan berat.


## Soal 3d 
## Analisa soal 3d:
Pada soal 3d ini diminta untuk bisa mengirimkan folder harta karun yang sudah terstruktur melalui program soal3.c, tetapi sebelum dikirim perlu untuk dizip telebih dahulu untuk pemindahannya.

## Cara Pengerjaan 3d:
Untuk bisa mengkoneksikan file client.c dan server.c  perlu mendeklrasikan port, IP, MAXBUF yang sama. Dan berikut program dari client untuk bisa mengkoneksikan pada server
```c
#define PORT    5500
#define IP      "127.0.0.1"
#define MAXBUF  1024

int main() {
    struct sockaddr_in server_address;
    int     s;
    int         sourse_fd;
    char        buf[MAXBUF];
    int         file_name_len, read_len;
    /* socket() */
    s = socket(AF_INET, SOCK_STREAM, 0);
    if(s == -1) {
        return 1;
    }
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(IP);
    server_address.sin_port = htons(PORT);

    if(connect(s, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("connect : ");
        printf("fail to connect.\n");
        close(s);
        return 1;
    }
```

## Hasil run 3d:
![user.txt](./img/3d.JPG)
## Kendala 3d:
Untuk kendala nomer ini yaitu pada awal tidak tahu cara mengkoneksikan ke dua file client dan server tersebut.




## Soal 3e
## Analisa soal 3e:
Pada soal 3e diminta agar bisa mengirimkan zip harta karun tersbut dengan command send hartakarun.zip

## Cara Pengerjaan 3e:
Untuk bisa mengirimkan harus mengrun file server terlebih dahulu. Kemudian mengrun file client dan nanti program akan meminta untuk mengirim file mana yang ingin dikirim. Dan berikut fungsi yang digunakan pada client untuk mengirim 
```c
 memset(buf, 0x00, MAXBUF);
    printf("write file name to send to the server:  ");
    scanf("%s", buf);

    printf(" > %s\n", buf);
    file_name_len = strlen(buf);

    send(s, buf, file_name_len, 0);
    sourse_fd = open(buf, O_RDONLY);
    if(!sourse_fd) {
        perror("Error : ");
        return 1;
    }

```
Server akan memberikan konfirmasi file yang dikirim dari client apakah sudah masuk dengan menggunakan 
```c
while(1) {
        char file_name[MAXBUF]; // local val
        memset(buf, 0x00, MAXBUF);

        /* accept() */
        client_sockfd = accept(server_sockfd, (struct sockaddr *)&clientaddr, &client_len);
        printf("New Client Connect : %s\n", inet_ntoa(clientaddr.sin_addr));

        /* file name */
        read_len = read(client_sockfd, buf, MAXBUF);
        if(read_len > 0) {
            strcpy(file_name, buf);
            printf("%s > %s\n", inet_ntoa(clientaddr.sin_addr), file_name);
        } else {
            close(client_sockfd);
            break;
        }

        /* create file */

        des_fd = open(file_name, O_WRONLY | O_CREAT | O_EXCL, 0700);
        if(!des_fd) {
            perror("file open error : ");
            break;
        }   
        /* file save */
        while(1) {
            memset(buf, 0x00, MAXBUF);
            file_read_len = read(client_sockfd, buf, MAXBUF);
            write(des_fd, buf, file_read_len);
            if(file_read_len == EOF | file_read_len == 0) {
                printf("finish file\n");
                break;
            }


        }


```
## Hasil run 3e:
![user.txt](./img/3e1.JPG)
![user.txt](./img/3e2.JPG)
![user.txt](./img/3e.JPG)
## Kendala 3e:
Untuk kendala nomer ini yaitu ketika mengubah algoritma menjadi send hartakarun.zip menjadi tidak bisa dikirim.


