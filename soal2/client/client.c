#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFF_SZ 1024

volatile sig_atomic_t flag = 0;

// * 1 means server is ready to serve */
int server_avail = 0;

//* Storing auth status of this client
int is_logged_in = 0;

int sockfd = 0;
char id[32];
char pass[32];
char buffer[BUFF_SZ] = {0};

void trim_endl(char *arr, int length) {
  for (size_t i = 0; i < length; i++) {  // trim \n
    if (arr[i] == '\n') {
      arr[i] = '\0';
      break;
    }
  }
}

void handle_exit(int sig) { flag = 1; }

void send_file(char *filepath) {
  FILE *file_fd = fopen(filepath, "r");
  char buffer[BUFF_SZ] = {0};
  if (file_fd != NULL) {
    printf("Sending file: %s\n", filepath);
  } else {
    printf("Failed to open file: %s\n", filepath);
    //* Sending dummy file to server to cancel receiving file
    send(sockfd, "code_err", 20, 0);
    return;
  }

  while (fgets(buffer, BUFF_SZ, file_fd) != NULL) {
    if (send(sockfd, buffer, sizeof(buffer), 0) == -1) {
      perror("[-]Error in sending file.");
      exit(1);
    }
    bzero(buffer, BUFF_SZ);
  }
  send(sockfd, "code_eof", 10, 0);
}

void receive_file(char *filepath) {
  FILE *file_fd = fopen(filepath, "w");
  char buffer[BUFF_SZ];
  int n;

  while (1) {
    n = recv(sockfd, buffer, BUFF_SZ, 0);
    if (strcmp(buffer, "code_eof") == 0) {
      break;
    }
    if (n > 0 && strcmp(buffer, "code_err") == 0) {
      break;
    }

    fprintf(file_fd, "%s", buffer);
    bzero(buffer, BUFF_SZ);
  }
  fclose(file_fd);
}

int is_exist(char *filepath) {
  int return_val = 0;
  FILE *fp = fopen(filepath, "r");
  if (fp != NULL) {
    return_val = 1;
    fclose(fp);
  }

  return return_val;
}

void add_command() {
  char pub[256];
  char year[10];
  char path[512];
  char filename[512];

  printf("problem: ");
  scanf("%s", pub);
  printf("author: ");
  scanf("%s", year);
  printf("Filepath: ");
  scanf("%s", path);

  //* Inform the server you're going to add
  strcpy(buffer, "add");
  send(sockfd, buffer, strlen(buffer), 0);

  //* Get filename from path
  bzero(buffer, BUFF_SZ);
  strcpy(buffer, path);
  char *token = strtok(buffer, "/");
  while (token != NULL) {
    strcpy(filename, token);
    token = strtok(NULL, "/");
  }

  if (!is_exist(path)) {
    printf("File did not exist!\n");

    bzero(buffer, BUFF_SZ);
    strcpy(buffer, "code_err");
    send(sockfd, buffer, strlen(buffer), 0);
    return;
  }

  bzero(buffer, BUFF_SZ);
  sprintf(buffer, "%s:%s:%s", filename, pub, year);
  send(sockfd, buffer, strlen(buffer), 0);

  bzero(buffer, BUFF_SZ);
  send(sockfd, buffer, strlen(buffer), 0);
  send_file(path);
}

void download_command(char *filename) {
  char buffer[BUFF_SZ] = {0};
  if (!filename) {
    printf("Usage: download <filename>\n");
    return;
  }

  int n;
  n = send(sockfd, "download", 10, 0);
  n = send(sockfd, filename, strlen(filename), 0);

  //* receive code from server
  n = recv(sockfd, buffer, BUFF_SZ, 0);

  if (strcmp(buffer, "code_err") == 0) {
    printf("Error downloading file, file may not be in the server\n");
    return;
  } else if (strcmp(buffer, "code_found") == 0) {
    printf("Downloading file...\n");
    receive_file(filename);
    printf("File downloaded!\n");
  } else {
    printf("else\n");
  }

  return;
}

void delete_command(char *filename) {
  char buffer[BUFF_SZ] = {0};
  if (!filename) {
    printf("Usage: delete <filename>\n");
    return;
  }

  int n;
  n = send(sockfd, "delete", 10, 0);
  n = send(sockfd, filename, strlen(filename), 0);

  bzero(buffer, BUFF_SZ);
  n = recv(sockfd, buffer, BUFF_SZ, 0);
  if (strcmp(buffer, "code_err") == 0) {
    printf("Error deleting file, file may not be in the server\n");
    return;
  } else {
    printf("%s\n", buffer);
    return;
  }
}

void see_command() {
  char buffer[BUFF_SZ] = {0};
  int n;

  n = send(sockfd, "see", 10, 0);
  bzero(buffer, BUFF_SZ);
  while (1) {
    n = recv(sockfd, buffer, BUFF_SZ, 0);

    if (strcmp(buffer, "code_eof") == 0) {
      break;
    }

    printf("%s", buffer);
    bzero(buffer, BUFF_SZ);
    usleep(1000);
  }
}

void auth_handler() {
  while (!server_avail) {
    //* wait
  }

  int cmd = 0;
  while (!is_logged_in) {
    printf(
        "--\n"
        "~~Landing Page~~\n"
        "1. Login\n"
        "2. Register\n"
        "Enter command!\n");
    scanf("%d", &cmd);

    if (cmd == 1) {
      printf(
          "\n--\n"
          "~~Login Page~~"
          "\n--\n");
      printf("Enter id: ");
      scanf("%s", id);
      printf("Enter password: ");
      scanf("%s", pass);

      memset(buffer, 0, BUFF_SZ);
      sprintf(buffer, "login-%s:%s", id, pass);

      send(sockfd, buffer, strlen(buffer), 0);
    } else if (cmd == 2) {
      printf(
          "\n--\n"
          "~~Register Page~~"
          "\n--\n");
      printf("Enter id: ");
      scanf("%s", id);
      printf("Enter password: ");
      scanf("%s", pass);
       if((pass >= '0' & pass <= '9') || (pass >= 'a' && pass <= 'z') || (pass >= 'A' && pass <= 'Z')){
if(strlen(pass)<=8){}
				 
				 else{
				 	printf("password belum memenuhi syarat");
				 }
}
else{
printf("password belum memenuhi syarat");
}
    
      char confirm_pass[32];
      printf("Confirm password: ");
      scanf("%s", confirm_pass);

      if (strcmp(confirm_pass, pass) != 0) {
        printf("Passwordz didn\'t match!\n");
        continue;
      }

      memset(buffer, 0, BUFF_SZ);
      sprintf(buffer, "register-%s:%s", id, pass);
      send(sockfd, buffer, strlen(buffer), 0);
    } else {
      printf(
          "----\n"
          "Invalid command!\n"
          "----\n");
    }
    sleep(1);
  }
}

void send_handler() {
  char message[BUFF_SZ] = {0};  
  char buffer[BUFF_SZ] = {0};   

  while (1) {
    if (!is_logged_in) {
      auth_handler();
    }
    //* Scan the string and trim the trailing \n
    fgets(message, BUFF_SZ, stdin);
    trim_endl(message, BUFF_SZ);

    //* Spliting command and args
    char *command = strtok(message, " ");
    char *args = strtok(NULL, " ");

    if (!command) {
      printf("Enter a command!\n");
    } else {
      sprintf(message, "%s", command);
    }

    //* Handling command
    if (strcmp(message, "exit") == 0) {
      break;
    } else if (strcmp(message, "add") == 0) {
      add_command();
    } else if (strcmp(message, "download") == 0) {
      download_command(args);
    } else if (strcmp(message, "see") == 0) {
      see_command();
    } else {
      sprintf(buffer, "%s", message);
      send(sockfd, buffer, strlen(buffer), 0);
    }

    bzero(message, BUFF_SZ);
    bzero(buffer, BUFF_SZ);
  }

  handle_exit(2);
}

void recv_handler() {
  char received[BUFF_SZ] = {0};
  while (1) {
    if (is_logged_in) {
      continue;
    }

    int receive = recv(sockfd, received, BUFF_SZ, 0);
    if (receive > 0) {
      if (!server_avail && strcmp(received, "code_go") == 0) {
        server_avail = 1;
        memset(received, 0, sizeof(received));
      }

      if (!is_logged_in && strcmp(received, "code_auth_success") == 0) {
        memset(received, 0, sizeof(received));
        printf(
            "\n--\n"
            "Auth successful, you're logged in!"
            "\n--\n");
        is_logged_in = 1;
        continue;
      } else if (!is_logged_in && strcmp(received, "code_auth_failed") == 0) {
        memset(received, 0, sizeof(received));
        printf(
            "\n--\n"
            "Auth failed!"
            "\n--\n");
        is_logged_in = 0;
        continue;
      }

      printf("> %s", received);
    } else if (receive == 0) {
      break;
    } else {
      // -1
    }
    memset(received, 0, sizeof(received));

    sleep(1);
  }
}

void error(const char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  char *ip = "127.0.0.1";
  int port = 6969;

  signal(SIGINT, handle_exit);

  struct sockaddr_in serv_addr;

  //* Socket settings
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(ip);
  serv_addr.sin_port = htons(port);

  int conn = connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
  if (conn < 0) {
    error("ERROR: connect\n");
  }

  pthread_t send_thread, rcv_thread;
  //* Handling send to server
  if (pthread_create(&send_thread, NULL, (void *)send_handler, NULL) != 0) {
    error("Error pthread send.");
  }

  //* Handling receive from server
  if (pthread_create(&rcv_thread, NULL, (void *)recv_handler, NULL) != 0) {
    error("Error pthread recv.");
  }

  //* Handling exit in main thread
  while (1) {
    if (flag) {
      printf("\nBye\n");
      break;
    }
  }

  close(sockfd);
  return 0;
}