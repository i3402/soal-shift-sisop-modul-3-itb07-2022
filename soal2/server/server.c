#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

//* Maximum clients
#define MAX_CLI 10
//* Buffer size
#define BUFF_SZ 1024

//* Race condition safe client count
static _Atomic int cli_count = 0;
//* Initial uid
static int uid = 10;

//* Struct for client
typedef struct {
  struct sockaddr_in addr;
  int sockfd;
  int uid;
  char credentials[BUFF_SZ];
} client_t;

//* global client and auth data
int current_uid = 0;
int is_logged_in = 0;

//* Array of connected clients
client_t* clients[MAX_CLI];

//* Mutex for avoiding race condition
pthread_mutex_t clients_lock = PTHREAD_MUTEX_INITIALIZER;

//* Adding clients to the array
void add_client(client_t* client) {
  pthread_mutex_lock(&clients_lock);

  for (size_t i = 0; i < MAX_CLI; i++) {
    if (!clients[i]) {
      clients[i] = client;
      break;
    }
  }

  pthread_mutex_unlock(&clients_lock);
}

//* Remove clients from the array
void remove_client(int uid) {
  pthread_mutex_lock(&clients_lock);
  //* implementing remove at index

  //* get index by uid
  int index = 0;
  for (int i = 0; i < MAX_CLI; ++i) {
    if (clients[i]->uid == uid) {
      break;
    }
    index++;
  }

  //* shift array
  for (size_t i = index; i < MAX_CLI; i++) {
    clients[i] = clients[i + 1];
  }

  clients[cli_count - 1] = NULL;

  pthread_mutex_unlock(&clients_lock);
}

void broadcast() {
  pthread_mutex_lock(&clients_lock);

  char buff_out[BUFF_SZ] = {0};
  int count = 0;

  for (size_t i = 0; i < MAX_CLI; i++) {
    if (clients[i] && current_uid != clients[i]->uid) {
      count++;
      if (count == 1 && current_uid == 0) {
        strcpy(buff_out, "code_go");
        send(clients[i]->sockfd, buff_out, strlen(buff_out), 0);
        current_uid = clients[i]->uid;
      } else {
        memset(buff_out, 0, sizeof(buff_out));
        sprintf(
            buff_out,
            "Someone has left, you are no %d in the queue, please wait...\n",
            count);
        send(clients[i]->sockfd, buff_out, strlen(buff_out), 0);
      }
    }
  }

  pthread_mutex_unlock(&clients_lock);
}

int auth(char* data, client_t* cli) {
  FILE* users_db;
  users_db = fopen("users.txt", "a+");
  char users_buff[64];
  strcpy(users_buff, data);

  char mode[10] = {0};
  char* token = strtok(users_buff, "-");
  strcpy(mode, token);

  token = strtok(NULL, "-");
  char acc_data[64] = {0};
  strcpy(acc_data, token);
  bzero(users_buff, 64);

  if (strcmp(mode, "login") == 0) {
    while (fscanf(users_db, "%s", users_buff) != EOF) {
      if (strcmp(acc_data, users_buff) == 0) {
        strcpy(cli->credentials, acc_data);
        printf("%s has joined!\n", cli->credentials);
        fclose(users_db);
        is_logged_in = 1;
        return 1;
      }
    }
  } else if (strcmp(mode, "register") == 0) {
    while (fscanf(users_db, "%s", users_buff) != EOF) {
      if (strcmp(acc_data, users_buff) == 0) {
        printf("Register failed, account exists!\n");
        fclose(users_db);
        return 0;
      }
    }

    if (fprintf(users_db, "%s\n", acc_data) < 0) {
      printf("Error appending account!\n");
      fclose(users_db);
      return 0;
    }

    printf("Register sucessfull!\n");
    is_logged_in = 1;
    fclose(users_db);
    strcpy(cli->credentials, acc_data);
    printf("%s has joined!\n", cli->credentials);
    return 1;
  }

  return 0;  // not found
}

int update_files_tsv(char* filename, char* problem, char* author) {
  FILE* db = fopen("problems.tsv", "a+");

  if (fprintf(db, "\nFILES/%s\t%s\t%s", filename, problem, author) < 0) {
    printf("Error appending file info!\n");
    fclose(db);
    return -1;
  }

  fclose(db);
  return 0;
}

void send_file(char* filepath, int sockfd) {
  FILE* file_fd = fopen(filepath, "r");
  char buffer[BUFF_SZ] = {0};

  if (file_fd != NULL) {
    printf("Sending file %s to client...\n", filepath);
  } else {
    printf("Failed to open file: %s\n", filepath);
    //* Sending dummy file to server to cancel receiving file
    send(sockfd, "code_err", 20, 0);
    return;
  }

  while (fgets(buffer, BUFF_SZ, file_fd) != NULL) {
    if (send(sockfd, buffer, BUFF_SZ, 0) == -1) {
      perror("[-]Error in sending file.");
      exit(1);
    }
    bzero(buffer, BUFF_SZ);
  }

  send(sockfd, "code_eof", 10, 0);
  fclose(file_fd);
}

void receive_file(char* filepath, int sockfd) {
  FILE* file_fd = fopen(filepath, "w");
  char buffer[BUFF_SZ];
  int n;
  while (1) {
    n = recv(sockfd, buffer, BUFF_SZ, 0);
    if (strcmp(buffer, "code_eof") == 0) {
      break;
    }
    if (n > 0 && strcmp(buffer, "code_err") == 0) {
      printf("up\n");
      break;
    }

    fprintf(file_fd, "%s", buffer);
    bzero(buffer, BUFF_SZ);
  }
  fclose(file_fd);
}

int find_file(char* filename) {
  FILE* db = fopen("problems.tsv", "r");
  char buffer[BUFF_SZ] = {0};
  char filepath[512] = {0};

  sprintf(filepath, "FILES/%s", filename);
  printf("Finding %s...\n", filepath);
  bzero(buffer, BUFF_SZ);

  int line = 0;
  while (fscanf(db, "%s\t%*s\t%*s", buffer) != EOF) {
    line++;

    if (strcmp(filepath, buffer) == 0) {
      fclose(db);
      return line;
    }
  }

  fclose(db);
  return -1;
}

void pretty_print(char* line, client_t* cli) {
  char buffer[BUFF_SZ] = {0};
  char filename[256] = {0};
  char fileext[6] = {0};
  char problem[256] = {0};
  char author[6] = {0};
  char filepath[256] = {0};
  int n;

  //* Split by tab
  sscanf(line, "%s\t%s\t%s", filepath, problem, author);

  //* remove FILES/
  bzero(buffer, BUFF_SZ);
  strcpy(buffer, filepath);
  char* path_token = strtok(buffer, "/");
  path_token = strtok(NULL, "/");  //* filename.ext

  // *split filename and ext
  char* name_ext_token = strtok(path_token, ".");
  strcpy(filename, name_ext_token);
  name_ext_token = strtok(NULL, ".");
  strcpy(fileext, name_ext_token);

  bzero(buffer, BUFF_SZ);
  sprintf(buffer,
          "Nama: %s\n"
          "problem: %s\n"
          "author: %s\n"
          "Extensi File: %s\n"
          "Filepath: %s\n\n",
          filename, problem, author, fileext, filepath);
  n = send(cli->sockfd, buffer, BUFF_SZ, 0);
}

void client_download(client_t* cli) {
  FILE* db = fopen("problems.tsv", "r");
  char buffer[BUFF_SZ] = {0};
  char filename[256] = {0};
  char filepath[512] = {0};
  int n;

  //* get filename from args
  n = recv(cli->sockfd, buffer, BUFF_SZ, 0);
  if (n <= 0) return;
  if (strcmp(buffer, "code_err") == 0) {
    printf("client send error\n");
    return;
  }

  strcpy(filename, buffer);
  bzero(buffer, BUFF_SZ);

  int is_valid = 0;
  //* the asterisk skips the string
  fscanf(db, "%*s\t%*s\t%*s");  //* skip header
  printf("Finding...\n");

  while (fscanf(db, "%s\t%*s\t%*s", buffer) != EOF) {
    strcpy(filepath, buffer);
    char* token = strtok(buffer, "/");
    token = strtok(NULL, "/");
    if (strcmp(token, filename) == 0) {
      is_valid = 1;
      break;
    }
  }

  if (is_valid) {
    printf("downloading %s\n", filepath);
    n = send(cli->sockfd, "code_found", 15, 0);
    send_file(filepath, cli->sockfd);
  } else {
    printf("%s is not found\n", filename);
    n = send(cli->sockfd, "code_err", 10, 0);
  }

  fclose(db);
}

void client_add(client_t* cli) {
  FILE* log = fopen("running.log", "a");
  char problem[256] = {0};
  char author[10] = {0};
  char filename[512] = {0};
  char info_buffer[BUFF_SZ] = {0};
  char buffer[BUFF_SZ * 2] = {0};

  int file_info = recv(cli->sockfd, buffer, BUFF_SZ, 0);
  if (file_info < 0) {
    printf("Error recieving file info!\n");
  } else if (file_info > 0 && strcmp(buffer, "code_err") == 0) {
    printf("client send error code!\n");
    // continue;
    fclose(log);
    return;
  }
  //* if not error
  else if (file_info > 0 && strcmp(buffer, "code_err") != 0) {
    strcpy(info_buffer, buffer);

    char* token = strtok(info_buffer, ":");
    strcpy(filename, token);
    token = strtok(NULL, ":");
    strcpy(problem, token);
    token = strtok(NULL, ":");
    strcpy(author, token);

    if (update_files_tsv(filename, problem, author) < 0) {
      bzero(buffer, BUFF_SZ);
      strcpy(buffer, "Failed to add data!\n");
      send(cli->sockfd, buffer, strlen(buffer), 0);
    } else {
      bzero(buffer, BUFF_SZ);
      strcpy(buffer, "Successfully added data to db\n");
      send(cli->sockfd, buffer, strlen(buffer), 0);
      printf("%s", buffer);

      bzero(buffer, BUFF_SZ);
      sprintf(buffer, "Tambah : %s (%s)\n", filename, cli->credentials);
      fputs(buffer, log);
    }
  }

  bzero(info_buffer, BUFF_SZ);
  sprintf(info_buffer, "FILES/%s", filename);

  receive_file(info_buffer, cli->sockfd);
  fclose(log);
}

void client_delete(client_t* cli) {
  FILE* db = fopen("problems.tsv", "r");
  FILE* log = fopen("running.log", "a");
  FILE* temp = fopen("temp.tsv", "w");
  char filename[512] = {0};
  char new_filename[512 + 10] = {0};
  char buffer[BUFF_SZ * 2] = {0};
  int n;

  //* receive filename
  n = recv(cli->sockfd, buffer, BUFF_SZ, 0);
  strcpy(filename, buffer);

  int line_no = find_file(filename);
  if (line_no < 0) {
    printf("%s is not found\n", filename);
    n = send(cli->sockfd, "code_err", 10, 0);
  } else {
    int count = 0;
    bzero(buffer, BUFF_SZ);
    printf("Deleted %s from line %d\n", filename, line_no);

    while (fgets(buffer, BUFF_SZ, db) != NULL) {
      count++;
      if (count == line_no) {
        continue;
      } else {
        /*
         * !!CAUTION WEIRD WORKAROUND!!
         * handle new line problem when deleting row in tsv
         */
        buffer[strlen(buffer) - 1] = '\0';
        char fix_newline[BUFF_SZ] = {0};
        if (count != 1) {
          strcpy(fix_newline, "\n");
          strcat(fix_newline, buffer);
          strcpy(buffer, fix_newline);
        }
        fputs(buffer, temp);
      }
    }

    bzero(buffer, BUFF_SZ);
    sprintf(new_filename, "FILES/old-%s", filename);
    sprintf(buffer, "FILES/%s", filename);
    rename(buffer, new_filename);

    remove("problems.tsv");
    rename("temp.tsv", "problems.tsv");

    bzero(buffer, BUFF_SZ);
    strcpy(buffer, "Successfully delete file!");
    n = send(cli->sockfd, buffer, strlen(buffer), 0);

    bzero(buffer, BUFF_SZ);
    sprintf(buffer, "Hapus : %s (%s)\n", filename, cli->credentials);
    fputs(buffer, log);
  }

  fclose(log);
  fclose(db);
  fclose(temp);

  return;
}

void client_find(client_t* cli) {
  FILE* db = fopen("problems.tsv", "r");
  char buffer[BUFF_SZ] = {0};
  char query[BUFF_SZ] = {0};
  char line_found[BUFF_SZ] = {0};
  char* substr = NULL;
  int n;

  n = recv(cli->sockfd, query, BUFF_SZ, 0);
  fgets(buffer, BUFF_SZ, db);

  while (fgets(buffer, BUFF_SZ, db) != NULL) {
    strcpy(line_found, buffer);
    char* path_tok = strtok(buffer, "\t");
    char* filename_tok = strtok(path_tok, "/");
    filename_tok = strtok(NULL, "/");

    substr = strstr(filename_tok, query);

    if (substr != NULL) {
      pretty_print(line_found, cli);
    }
  }

  if (substr == NULL) {
    printf("Not Found\n");
    n = send(cli->sockfd, "code_404", 10, 0);
  } else {
    printf("Found\n");
    n = send(cli->sockfd, "code_eof", 10, 0);
  }

  fclose(db);
}

void client_see(client_t* cli) {
  FILE* db = fopen("problems.tsv", "r");
  char buffer[BUFF_SZ * 2] = {0};
  int n;

  printf("Sending database data to client...\n");

  //* skips header
  fgets(buffer, BUFF_SZ, db);

  while (fgets(buffer, BUFF_SZ, db)) {
    pretty_print(buffer, cli);
  }

  n = send(cli->sockfd, "code_eof", 10, 0);
  fclose(db);
}

//* ELEPHANT IN THE ROOM!!
void* handle_client(void* client) {
  client_t* cli = (client_t*)client;

  char buff_out[BUFF_SZ * 2];
  int recv_retval = 0;
  int leave_flag = 0;

  //* Client behaviour when initially connected
  if (cli_count > 0) {
    //* send signal to any other client in queue
    bzero(buff_out, BUFF_SZ);
    sprintf(buff_out,
            "Server is busy, you are in queue no %d, please wait...\n",
            cli_count);
    send(cli->sockfd, buff_out, strlen(buff_out), 0);
  } else {
    //* send signal to first in queue client
    bzero(buff_out, BUFF_SZ);
    strcpy(buff_out, "code_go");
    send(cli->sockfd, buff_out, strlen(buff_out), 0);
    current_uid = cli->uid;
  }

  cli_count++;
  printf("Currently connected client: %d\n", cli_count);

  //* Handling incoming msg from client
  while (1) {
    if (leave_flag) {
      break;
    }

    bzero(buff_out, BUFF_SZ);
    recv_retval = recv(cli->sockfd, buff_out, BUFF_SZ, 0);

    //* if error
    if (recv_retval < 0) {
      printf("Error handling incoming msg\n");
      leave_flag = 1;
      continue;
    }

    //* If client sends empty command or send an "exit"
    if (recv_retval == 0 || strcmp(buff_out, "exit") == 0) {
      //* print id:pass is client logged in
      if (strlen(cli->credentials) > 0) {
        printf("%s has left!\n", cli->credentials);
      } else {
        printf("Someone has left!\n");
      }

      //* reset current_uid and logged_in flag
      if (current_uid == cli->uid) {
        current_uid = 0;
        is_logged_in = 0;
      }

      //* server new client if exist, wait for new conn if not
      if (current_uid != 0) {
        printf("Current user is: %d\n", current_uid);
      } else {
        printf("The client queue is empty, waiting for connection...\n");
      }

      leave_flag = 1;
      continue;
    }

    //* If client sends an auth command
    if (recv_retval > 0 && !is_logged_in) {
      if (!is_logged_in) {
        //* authentication process
        int valid = auth(buff_out, cli);

        bzero(buff_out, BUFF_SZ);
        if (valid) {
          strcpy(buff_out, "code_auth_success");
        } else {
          strcpy(buff_out, "code_auth_failed");
        }
        send(cli->sockfd, buff_out, strlen(buff_out), 0);
      }
      continue;
    }
    //* If client wants to add
    if (recv_retval > 0 && strcmp(buff_out, "add") == 0) {
      client_add(cli);
      continue;
    }
    //* If client wants to download
    if (recv_retval > 0 && strcmp(buff_out, "download") == 0) {
      client_download(cli);
      continue;
    }
    //* If client wants to see
    if (recv_retval > 0 && strcmp(buff_out, "see") == 0) {
      client_see(cli);
      continue;
    }
    //* If client send something else
    if (recv_retval > 0) {
      printf("client: %s\n", buff_out);
      continue;
    }

    bzero(buff_out, BUFF_SZ);
  }

  //* Delete client from queue and cleanup
  close(cli->sockfd);
  remove_client(cli->uid);
  free(cli);
  cli_count--;
  broadcast();
  pthread_detach(pthread_self());

  return NULL;
}

void error(const char* msg) {
  perror(msg);
  exit(1);
}

int main() {
  char* ip = "127.0.0.1";
  int port = 6969;
  int opt = 1;
  int listenfd = 0, connfd = 0;
  struct sockaddr_in serv_addr, cli_addr;
  errno = 0;
  pthread_t tid;

  listenfd = socket(AF_INET, SOCK_STREAM, 0);

  if (listenfd < 0) error("ERROR: Error opening socket!");
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(ip);
  ;
  serv_addr.sin_port = htons(port);

  printf("Server listeing on %s:%d\n", ip, port);

  //* Set option
  if (setsockopt(listenfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR),
                 (char*)&opt, sizeof(opt)) < 0) {
    error("ERROR: setsockopt failed!");
  }

  //* Bind
  if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
    error("ERROR: Socket binding failed");
  }

  //* Listen
  if (listen(listenfd, 10) < 0) {
    error("ERROR: Socket listening failed");
  }
  //* Checking users.txt
  if (access("users.txt", F_OK) != 0) {
    printf("users.txt is not found, creating...\n");
  }
  FILE* users_db = fopen("users.txt", "a+");
  fclose(users_db);

  //* Checking problems.tsv
  FILE* file_db;
  if (access("problems.tsv", F_OK) != 0) {
    file_db = fopen("problems.tsv", "a+");
    if (file_db != NULL) {
      printf("problems.tsv is not found, creating...\n");
      fputs("path\tproblem\tauthor", file_db);
    }
    fclose(file_db);
  }

  //* Running logs
  FILE* log;
  if (access("running.log", F_OK) != 0) {
    printf("running.log is not found, creating...\n");
  }
  // log = fopen("running.log", "a+");
  // fclose(log);

  while (1) {
    socklen_t clilen = sizeof(cli_addr);
    //* Every new connection is stored in connfd first
    connfd = accept(listenfd, (struct sockaddr*)&cli_addr, &clilen);

    if ((cli_count + 1) == MAX_CLI) {
      printf("Max clients reached. Rejected: ");
      printf(":%d\n", cli_addr.sin_port);
      close(connfd);
      continue;
    }

    //* Client settings
    client_t* cli = (client_t*)malloc(sizeof(client_t));
    cli->addr = cli_addr;
    cli->sockfd = connfd;
    cli->uid = uid++;

    //* Add client to the queue and create new thread
    add_client(cli);
    pthread_create(&tid, NULL, &handle_client, (void*)cli);

    sleep(1);
  }

  return 0;
}